<?php
/**
 * Created by ntuanphuc.
 * User: Passion
 * Date: 4/20/19
 * Time: 23:24
 */

namespace ytdm\todos;

use Illuminate\Support\ServiceProvider;

class TodosServiceProvider extends ServiceProvider
{
    public function boot(){
        //routes
        $this->loadRoutesFrom(__DIR__.'/routes/web.php');
        $this->loadViewsFrom(__DIR__.'/resources/views', 'todos');
        $this->loadMigrationsFrom(__DIR__.'/Database/migrations');
    }


    public function register(){

    }
}