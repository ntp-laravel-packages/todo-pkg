@extends('layouts.app')
@section('content')
<div class="container">
    @if(session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

    {{--<h3>Your todo list</h3>--}}

        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Your Todo list</div>

                    <div class="card-body">
                            @if(count($todos))
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Title</th>
                                        <th>Detail</th>
                                        <th>Created At</th>
                                        <th>Last Update</th>
                                        <th>Edit</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @php
                                        $i = 1;
                                    @endphp
                                    @foreach($todos as  $todo)
                                        <tr>
                                            <td>{{ $i }}</td>
                                            <td>{{ $todo->title }}</td>
                                            <td>{{ $todo->detail }}</td>
                                            <td>{{ date("Y-m-d H:i", strtotime($todo->created_at)) }}</td>
                                            <td>{{ date("Y-m-d H:i", strtotime($todo->updated_at)) }}</td>
                                            <td><a href="{{ route('todo.update', ['id' => $todo->id]) }}">Edit</a></td>
                                            <td></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @else
                                There is no Todo in your list. <a href="{{ route('todo.create') }}">Add one</a>
                            @endif

                    </div>
                </div>
            </div>
        </div>

</div>
@endsection