@extends('layouts.app')
@section('content')
    <div class="container">
        @if(session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Create your todo every day</div>
                    <div class="card-body">
                        <form action="{{route('todo.create')}}" method="POST">
                            @if ($errors->any())
                                <div class="alert alert-danger" role="alert">
                                    Please fix the following errors
                                </div>
                            @endif
                            @if($errors->any())
                                @foreach ($errors->all() as $error)
                                    <div>{{ $error }}</div>
                                @endforeach
                            @endif

                            @csrf
                            <div class="form-group">
                                <label for="exampleFormControlInput1">Title</label>
                                <input type="text" class="form-control" name="title" id="exampleFormControlInput"
                                       placeholder="Coding">
                            </div>
                            {{--<div class="form-group">
                                <label for="exampleFormControlInput1">Email address</label>
                                <input type="email" class="form-control" name="email" id="exampleFormControlInput1" placeholder="name@example.com">
                            </div>--}}

                            <div class="form-group">
                                <label for="exampleFormControlTextarea1">Enter Detail</label>
                                <textarea class="form-control" name="detail" id="exampleFormControlTextarea1"
                                          rows="5"></textarea>
                            </div>

                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection