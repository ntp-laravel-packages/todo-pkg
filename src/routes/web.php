<?php
/**
 * Created by ntuanphuc.
 * User: Passion
 * Date: 4/21/19
 * Time: 00:25
 */

Route::group(['namespace' => 'ytdm\todos\Http\Controllers', 'middleware' => ['web']], function(){
    Route::get('todo', 'TodoController@index')->name('todo.list');
    Route::match(['get','post'],'todo/create', 'TodoController@create')->name('todo.create');
    Route::match(['get','post'],'todo/update/{id}', 'TodoController@update')->where(['id' => '[0-9]+'])->name('todo.update');
});