<?php
/**
 * Created by ntuanphuc.
 * User: Passion
 * Date: 4/21/19
 * Time: 09:06
 */
namespace ytdm\todos\Models;
use Illuminate\Database\Eloquent\Model;

class Todos extends Model
{
    protected  $table = 'todos';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'desc','user_id'
    ];
}