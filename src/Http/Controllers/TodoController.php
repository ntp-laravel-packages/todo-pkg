<?php
/**
 * Created by ntuanphuc.
 * User: Passion
 * Date: 4/21/19
 * Time: 08:59
 */
namespace ytdm\todos\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use ytdm\todos\Models\Todos;

class TodoController extends Controller
{
    function __construct(){
        $this->middleware('auth');
    }
    function index(Request $request){
        $todos = Todos::all();
        return view('todos::category.index', ['todos' => $todos]);
    }
    function create(Request $request){
        if($request->isMethod('post')){
            $user = auth()->user();
            $data = $request->validate([
                'title' => 'required'
            ]);
            $data['user_id'] = $user->id;
            $todo = new Todos($data);
            $todo->save();
            return redirect(route('todo.list'));
        }
        return view('todos::category.create');
    }

    function update(Request $request, $id){
        $todo = Todos::find($id);
        if($todo === null){
            abort(404);
        }
        $user = auth()->user();
        if($user->id != $todo->user_id){
            abort(404);
        }
        if($request->isMethod('post')){
            $request->validate([
                'title' => 'required'
            ]);
            $todo->title = $request->get('title');
            $todo->detail = $request->get('detail');
            $todo->save();
            return redirect(route('todo.list'));
        }
        return view('todos::category.update', ['todo' => $todo]);
    }
}